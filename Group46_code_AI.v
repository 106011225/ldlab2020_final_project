// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01

// define bit width
`define NUM_PARAMS_BITS 25 

module AIplayer(
    input wire clk,
    input wire rst,
    input wire req,
    input wire [0 : 2*`BOARD_H*`BOARD_W-1] Board,
    output reg [0 : 2*`BOARD_H*`BOARD_W-1] newBoard,
    output reg valid
);


    // FSM
    parameter INIT = 3'b000;
    parameter CHECK_CRITICAL_MOVE = 3'b001;
    parameter FORWARD_NN = 3'b010;
    parameter NN_MOVE = 3'b011;
    parameter OUTP = 3'b111;
    reg [2:0] state, next_state;

    // flag
    reg isCritical, next_isCritical;//

    // output
    reg [0 : 2*`BOARD_H*`BOARD_W-1] next_newBoard;
    reg next_valid;

    reg [5:0] timer, next_timer;
    

    // NN
    reg [2:0] testingCol, next_testingCol;
    reg isProcessing, next_isProcessing;
    reg [`NUM_PARAMS_BITS-1 : 0] currMax, next_currMax;
    reg [2:0] currOptCol, next_currOptCol;
    reg [0 : 2*`BOARD_H*`BOARD_W-1] NNinputBoard, next_NNinputBoard;
    reg reqNN, next_reqNN;
    wire [`NUM_PARAMS_BITS-1 : 0] y;
    wire validNN;
    reg [0:3*7-1] colHeight, next_colHeight;

    integer i;
    NN n0(.clk(clk), .rst(rst), .req(reqNN), .Board(NNinputBoard), .y(y), .valid_1p(validNN));
            
    


    // DFF
    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            state = INIT;
            newBoard = 84'b0;
            valid = 0;
            isCritical = 0;
            testingCol = 0;
            isProcessing = 0;
            currMax = 0;
            currOptCol = 0;
            NNinputBoard = 0;
            reqNN = 0;
            colHeight = 0;
            timer = 0;
        end
        else begin
            state = next_state;
            newBoard = next_newBoard;
            valid = next_valid;
            isCritical = next_isCritical;
            testingCol = next_testingCol;
            isProcessing = next_isProcessing;
            currMax = next_currMax;
            currOptCol = next_currOptCol;;
            NNinputBoard = next_NNinputBoard;
            reqNN = next_reqNN;
            colHeight = next_colHeight;
            timer = next_timer;
        end 
    end


    // combinational
    always @ (*) begin 
        next_state = state;
        next_newBoard = newBoard;
        next_valid = valid;
        next_isCritical = isCritical;
        next_testingCol = testingCol;
        next_isProcessing = isProcessing;
        next_currMax = currMax;
        next_currOptCol = currOptCol;
        next_NNinputBoard = NNinputBoard;
        next_reqNN = reqNN;
        next_colHeight = colHeight;
        next_timer = timer;

        case(state) 
            INIT: begin
                if (req) begin
                    next_state = CHECK_CRITICAL_MOVE;
                end
            end
            CHECK_CRITICAL_MOVE: begin

                next_state = FORWARD_NN; // by default
                next_testingCol = 3'b111; // -1
                next_isProcessing = 0;
                next_currMax = 25'b1111111111111111111111111;
                next_currOptCol = 0;
                next_reqNN = 0;
                for (i = 0; i < 7; i=i+1) begin
                    next_colHeight[i*3 +: 3] = Board[1+i*2] + Board[15+i*2] + Board[29+i*2] + Board[43+i*2] + Board[57+i*2] + Board[71+i*2];
                end

                next_newBoard = Board;
                Task_checkCritical;
                /* if isCritical:
                    next_newBoard[...] = `STONE_AI___;
                    next_isCritical = 1;
                    next_state = OUTP;
                    next_valid = 1;
                */
                            
            end
            FORWARD_NN: begin

                // forward pass
                if (isProcessing) begin
                    if (validNN) begin // done of calculating
                        next_reqNN = 0;
                        next_isProcessing = 0;

                        if ((y[`NUM_PARAMS_BITS-1] == currMax[`NUM_PARAMS_BITS-1]) && (y > currMax) ) begin
                            next_currMax = y;
                            next_currOptCol = testingCol;
                        end
                        else if ((y[`NUM_PARAMS_BITS-1] == 0) && (currMax[`NUM_PARAMS_BITS-1]==1)) begin
                            next_currMax = y;
                            next_currOptCol = testingCol;
                        end
                    end
                end
                else begin // not isProcessing
                    // test another col 
                    if (testingCol == 3'b111 || testingCol < 6) begin
                        next_testingCol =  testingCol + 1;

                        if (colHeight[testingCol+1] < 6 && Board[((colHeight[testingCol+1])*7 + (testingCol+1))*2 +: 2]==`STONE_BLANK) begin
                            next_NNinputBoard = Board;
                            next_NNinputBoard[((colHeight[testingCol+1])*7 + (testingCol+1))*2 +: 2] = `STONE_AI___;
                            next_reqNN = 1;
                            next_isProcessing = 1;
                        end
                    end
                    // done, trans to next state
                    else if (testingCol == 6) begin
                        next_state = NN_MOVE;
                    end
                end
                
            end
            NN_MOVE: begin
                next_newBoard = Board;
                next_newBoard[((colHeight[currOptCol])*7 + (currOptCol)) +: 2] = `STONE_AI___;
                next_state = OUTP;
                next_valid = 1;
            end
            OUTP: begin
                if (timer < 31)
                    next_timer = timer + 1;
                else begin
                    next_state = INIT;
                    next_valid = 0;
                    next_timer = 0;
                end

            end
            default: begin
                next_state = INIT;
            end
        endcase
    end

//end of module

    task Task_checkCritical;
    begin
            
        // vertical
        if((Board[42 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[14 +: 2] && Board[14 +: 2] == Board[28 +: 2])) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[48 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[34 +: 2])) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 0 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 0 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 2 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 4 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 2 +: 2] && Board[ 2 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 2 +: 2] && Board[ 2 +: 2] == Board[ 4 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[14 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 0 +: 2] != 0)) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[20 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[18 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[14 +: 2] != 0)) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 0 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2])) begin
            next_newBoard[ 0 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[48 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[32 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[ 6 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[42 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[42 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[42 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[30 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[44 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[30 +: 2])) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[50 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[36 +: 2])) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 2 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 4 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[22 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 2 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[50 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[34 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[ 8 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[44 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[32 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[46 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[32 +: 2])) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[52 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[38 +: 2])) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 4 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[10 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[24 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 4 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[52 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[36 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[10 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[46 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[34 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[48 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[34 +: 2])) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[54 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[26 +: 2] && Board[26 +: 2] == Board[40 +: 2])) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 6 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[10 +: 2] && Board[10 +: 2] == Board[12 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[10 +: 2] && Board[10 +: 2] == Board[12 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[10 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[12 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[12 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[12 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[26 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[26 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[12 +: 2] != 0)) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[26 +: 2] != 0)) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 6 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[54 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[54 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[54 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[38 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[12 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2])) begin
            next_newBoard[12 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[48 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[36 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[56 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[28 +: 2] && Board[28 +: 2] == Board[42 +: 2])) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[62 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[48 +: 2])) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[14 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[20 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[18 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[14 +: 2] != 0)) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[62 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[60 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[14 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2])) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[62 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[46 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[20 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[56 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[56 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[56 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[58 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[44 +: 2])) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[64 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[50 +: 2])) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[16 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[22 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[64 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[16 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[64 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[22 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[58 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[60 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[46 +: 2])) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[66 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[52 +: 2])) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[18 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[24 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[66 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[18 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[66 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[24 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[60 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[62 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[48 +: 2])) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[68 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[40 +: 2] && Board[40 +: 2] == Board[54 +: 2])) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[20 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[26 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[26 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[26 +: 2] != 0)) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[64 +: 2] != 0) && (Board[64 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[68 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[20 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[68 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[68 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[68 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[26 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2])) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[62 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[50 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[70 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[42 +: 2] && Board[42 +: 2] == Board[56 +: 2])) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[76 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[62 +: 2])) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[62 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[60 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[70 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[56 +: 2] != 0)) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[72 +: 2] && Board[72 +: 2] == Board[76 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[72 +: 2] && Board[72 +: 2] == Board[74 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[28 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[76 +: 2])) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[76 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[76 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[60 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[34 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[70 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[70 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[70 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[70 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2]) && (Board[56 +: 2] != 0)) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[72 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[58 +: 2])) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[78 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[64 +: 2])) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[64 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[78 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[30 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[78 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[78 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[78 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[36 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[72 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[72 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[72 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[74 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[60 +: 2])) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[80 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[66 +: 2])) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[66 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[80 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[32 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[80 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[80 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[80 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[38 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[74 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[74 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[74 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[76 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[62 +: 2])) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[82 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[54 +: 2] && Board[54 +: 2] == Board[68 +: 2])) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[64 +: 2] != 0) && (Board[64 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[68 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[78 +: 2] != 0) && (Board[78 +: 2] == Board[80 +: 2] && Board[80 +: 2] == Board[82 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[80 +: 2] && Board[80 +: 2] == Board[82 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[82 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[82 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[68 +: 2] != 0)) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[34 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[82 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[82 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[82 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[82 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2]) && (Board[68 +: 2] != 0)) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[40 +: 2] == 0) && (Board[52 +: 2] != 0) && (Board[52 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[76 +: 2])) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[76 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[76 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[64 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end

    end // end of task
    endtask

endmodule
`define BLANK 2'd0
`define YELLOW 2'd3		// AI
`define RED 2'd1		// human

module boardStatus (
	input [0:83] board,
	output reg [1:0] win
);
	// 0: no one win, 1: human win, 2: AI win
	parameter HUMAN = 1;
	parameter AI = 2;

	integer i;
	integer j;

	always @(*) begin		// detect win condition
		win = 0;

		for (i = 70; i >= 0; i = i - 14) begin		// horizontal condition
			for (j = i; j <= (i+6); j = j+2) begin	// col 1 ~ 4
				if (board[j+:2] == `YELLOW && board[(j+2)+:2] == `YELLOW && board[(j+4)+:2] == `YELLOW && board[(j+6)+:2] == `YELLOW) win = AI;
				else if (board[j+:2] == `RED && board[(j+2)+:2] == `RED && board[(j+4)+:2] == `RED && board[(j+6)+:2] == `RED) win = HUMAN;
			end
		end

		for (i = 70; i >= 42; i = i - 14) begin		// veritcal condition
			for (j = i; j <= (i+12); j = j+2) begin	// row 1 ~ 3
				if (board[j+:2] == `YELLOW && board[(j-14)+:2] == `YELLOW && board[(j-28)+:2] == `YELLOW && board[(j-42)+:2] == `YELLOW) win = AI;
				else if (board[j+:2] == `RED && board[(j-14)+:2] == `RED && board[(j-28)+:2] == `RED && board[(j-42)+:2] == `RED) win = HUMAN;
			end
		end

		for (i = 70; i >= 42; i = i - 14) begin		// 左上右下
			for (j = i; j <= (i+6); j = j+2) begin	// (1,1) ~ (4,3)
				if (board[j+:2] == `YELLOW && board[(j-12)+:2] == `YELLOW && board[(j-24)+:2] == `YELLOW && board[(j-36)+:2] == `YELLOW) win = AI;
				else if (board[j+:2] == `RED && board[(j-12)+:2] == `RED && board[(j-24)+:2] == `RED && board[(j-36)+:2] == `RED) win = HUMAN;
			end
		end

		for (i = 76; i >= 48; i = i - 14) begin		// 右上左下
			for (j = i; j <= (i+6); j = j+2) begin	// (4,1) ~ (7,3)
				if (board[j+:2] == `YELLOW && board[(j-16)+:2] == `YELLOW && board[(j-32)+:2] == `YELLOW && board[(j-48)+:2] == `YELLOW) win = AI;
				else if (board[j+:2] == `RED && board[(j-16)+:2] == `RED && board[(j-32)+:2] == `RED && board[(j-48)+:2] == `RED) win = HUMAN;
			end
		end
	end
endmodulemodule Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num = 0;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule
module debounce1p #(parameter shamt = 10) (
	input clk,
	input bef,
	output aft
);
	reg [shamt:0] shift_reg;

	always @(posedge clk) begin
		shift_reg [shamt:1] = shift_reg[shamt-1:0];
		shift_reg [0] = bef;
	end

	assign aft = (shift_reg == {1'b0, {shamt{1'b1}}}) ? 1 : 0;
endmodulemodule Display (clock, turn, digit, display, reset);
    input clock, reset;
    input [1:0] turn;
    output reg [3:0] digit;
    output reg [6:0] display;

    reg [3:0] bcd0, bcd1, bcd2, bcd3;
    reg [3:0] value;

    wire clk_div13;
    Clock_divider #(13) c0(.clk_in(clock), .clk_out(clk_div13));
    
    // HM / AI
    // 43   21
    always @ (*) begin
            bcd0 = (turn == 2'b11) ? 1: 0;
            bcd1 = (turn == 2'b11) ? 2: 0;
            bcd2 = (turn == 2'b01) ? 3: 0; 
            bcd3 = (turn == 2'b01) ? 4: 0;
    end

    always @ (posedge clk_div13, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b0000;
            value = 0;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd2;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd3;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1111111;// nothing 
            4'd1: display = 7'b1001111; //I
            4'd2: display = 7'b0001000; // A
            4'd3: display = 7'b1101010; // M 
            4'd4: display = 7'b0001001; // H
            default: display = 7'b1111111;
        endcase
    end

endmodule

module inputDetect (		// no problem
	inout PS2_DATA,
	inout PS2_CLK,
	input clk,
	input rst,
    output reg [2:0] putStone,
	output reg en
);

	wire [7:0] key_in;
	wire is_extend, is_break, valid, err;

	KeyboardCtrl_0 inst (
		.key_in(key_in),
		.is_extend(is_extend),
		.is_break(is_break),
		.valid(valid),
		.err(err),
		.PS2_DATA(PS2_DATA),
		.PS2_CLK(PS2_CLK),
		.rst(rst),
		.clk(clk)
	);

    always @(*) begin
		putStone = 0;
		en = 0;
        if (is_extend == 0 && is_break == 0 && valid == 1) begin
			case (key_in)
				8'h16: putStone = 1;
				8'h1E: putStone = 2;
				8'h26: putStone = 3;
				8'h25: putStone = 4;
				8'h2E: putStone = 5;
				8'h36: putStone = 6;
				8'h3D: putStone = 7;
				8'h5A: en = 1;
				default: begin
					putStone = 0;
					en = 0;
				end
			endcase
		end
    end
endmodulemodule ledCtrl (		// no bug
	input clk,
	input rst,
	input [1:0] win,
	output reg [15:0] led
);
	reg [15:0] next_led;

	always @(posedge clk or posedge rst) begin
		if (rst == 1) led = 16'b0;
		else led = next_led;
	end

	always @(*) begin
		next_led = led;
		case (win)
			1: begin		// player win
				if (led[7] == 0) begin
					next_led[7:1] = led[6:0];
					next_led[0] = 1'b1;
				end
			end
			2: begin
				if (led[8] == 0) begin
					next_led[14:8] = led[15:9];
					next_led[15] = 1'b1;
				end
			end
		endcase
	end
endmodule// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01

// define bit width
`define NUM_PARAMS_BITS 25 
`define NUM_CAL_BITS 62

`define SCALING_POW 24

// structure of NN
`define NUM_INPUT 42
`define NUM_HIDDEN_NODE 40
`define NUM_LAYER1_W 1680 // 42*40
`define NUM_LAYER1_B 40
`define NUM_LAYER2_W 40 // 40*1
`define NUM_LAYER2_B 1
`define NUM_TOTAL_PARAMS 1761

// base addr 
`define BASE_LAYER1_W 0
`define BASE_LAYER1_B 1680
`define BASE_LAYER2_W 1720
`define BASE_LAYER2_B 1760

module NN( 
    input wire clk,
    input wire rst,
    input wire req, // when we need to forward the NN, req flip to 1 w/ onepulse
    input wire [0 : 2*`BOARD_H*`BOARD_W-1] Board,
       // Board[row*7*2 + 2*col : row*7*2 + 2*col+1] = Board[row][col:col+1], store serially: [0][0:1], [0][2:3], ..., [5][12:13]
    output wire [`NUM_PARAMS_BITS-1 : 0] y,
    output wire valid_1p
);


    //clock
    wire clk_div2;

    // wire for reading mem of params 
    reg [10:0] addr, next_addr;
    wire [24:0] data;
    reg [10:0] addr_tmp;
    reg [10:0] addr_delay;

    //wire for calculating
    reg [`NUM_CAL_BITS-1 : 0] hidden_node [0 : `NUM_HIDDEN_NODE-1];
    reg [`NUM_CAL_BITS-1 : 0] next_hidden_node [0 : `NUM_HIDDEN_NODE-1];
    reg [`NUM_CAL_BITS-1 : 0] longy, next_longy;



    // output
    reg next_valid;
    reg valid; 

    // FSM
    parameter INIT = 2'b00;
    parameter CALC = 2'b01;
    parameter OUTP = 2'b10;
    reg [1:0] state, next_state;
    
    // use for for-loop
    integer i;

    Clock_divider #(2) c0(.clk_in(clk), .clk_out(clk_div2));
    Onepulse o0(.out(valid_1p), .in(valid), .clock(clk)); // to original clock domain
    blk_mem_gen_NNparams b0(.clka(clk), .wea(1'b0), .addra(addr), .dina(25'b0), .douta(data));

    assign y = longy[`NUM_PARAMS_BITS-1 : 0];

    // delayed signal of addr
    always @ (posedge clk) begin
        addr_tmp <= addr;
        addr_delay <= addr_tmp;
    end

    // DFF
    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) hidden_node[i] = 0;
            state = INIT;
            addr = 0;
            longy = 0;
            valid = 0;
        end
        else begin
            for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) hidden_node[i] = next_hidden_node[i];
            state = next_state;
            addr = next_addr;
            longy = next_longy;
            valid = next_valid;
        end
    end

    // combinational
    always @ (*) begin
        for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) next_hidden_node[i] = hidden_node[i];
        next_state = state;
        next_addr = addr;
        next_longy = longy;
        next_valid = valid;

        case(state)
            INIT: begin
                next_addr = 0;
                next_longy = 0;
                for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) next_hidden_node[i] = 0;

                if (req) begin
                    next_state = CALC;
                end
            end
            CALC: begin
                if (addr < `NUM_TOTAL_PARAMS + 2) begin // delay 2 cycle
                    next_addr = addr + 1;

                    if (addr_delay < `NUM_LAYER1_W) begin // layer1 weight
                        // hidden node += w * Xi with sign extension
                        next_hidden_node[ addr_delay/`NUM_INPUT] = hidden_node[ addr_delay/`NUM_INPUT] +
                                                             { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } * 
                                                             {
                                                                { (`NUM_CAL_BITS-1){ Board[ 2 * ( 7*((addr_delay%`NUM_INPUT)%`BOARD_H)+(addr_delay%`NUM_INPUT)/`BOARD_H ) ] } } ,
                                                                Board[2 * ( 7*((addr_delay%`NUM_INPUT)%`BOARD_H)+(addr_delay%`NUM_INPUT)/`BOARD_H ) + 1] 
                                                             } ;
                    end
                    else if (addr_delay < `NUM_LAYER1_W + `NUM_LAYER1_B) begin // layer1 bias & ReLU
                        next_hidden_node[ addr_delay - `BASE_LAYER1_B] = hidden_node[ addr_delay - `BASE_LAYER1_B] + 
                                                                  { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } ;
                        if (next_hidden_node[ addr_delay - `BASE_LAYER1_B][61] == 1)
                            next_hidden_node[ addr_delay - `BASE_LAYER1_B] = 0;
                    end
                    else if (addr_delay < `NUM_LAYER1_W + `NUM_LAYER1_B + `NUM_LAYER2_W) begin // layer2 weight
                        next_longy = longy + 
                                     { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } * 
                                     hidden_node[ addr_delay - `BASE_LAYER2_W ];
                    end
                    else begin // addr_delay == 1760, layer2 bias
                        // shift right first...
                        next_longy = (longy >> `SCALING_POW) + 
                                     { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } ;
                    end
                end
                else begin // addr >= `NUM_TOTAL_PARAMS
                    next_state = OUTP;
                    next_valid = 1;
                    next_addr = 0;
                end
            end
            OUTP: begin
                next_state = INIT;
                next_valid = 0;
            end
            default: begin
                next_state = INIT;
            end
        endcase
    end

endmodule

module Onepulse(out, in, clock);
    input in, clock;
    output reg out;

    reg in_delayn;

    always @ (posedge clock) begin
        in_delayn <= ~in;

        if (in & in_delayn)
            out <= 1;
        else 
            out <= 0;
    end
endmodule
`define BLANK 2'd0
`define RED 2'd1		// human

module Player (
	input [2:0] putStone,
	input [0:83] board_i,
	output reg [0:83] board_o,
	output valid
);
	integer i;

	reg err, placed;

	always @(*) begin
		err = 0;
		placed = 0;
		board_o = board_i;
		case (putStone)
			1: begin
				for (i = 0; i <= 70; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 70) err = 1;
				end
			end
			2: begin
				for (i = 2; i <= 72; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 72) err = 1;
				end
			end
			3: begin
				for (i = 4; i <= 74; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 74) err = 1;
				end
			end
			4: begin
				for (i = 6; i <= 76; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 76) err = 1;
				end
			end
			5: begin
				for (i = 8; i <= 78; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 78) err = 1;
				end
			end
			6: begin
				for (i = 10; i <= 80; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 80) err = 1;
				end
			end
			7: begin
				for (i = 12; i <= 82; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = `RED;
						placed = 1;
					end else if (placed == 0 && i == 82) err = 1;
				end
			end
		endcase
	end

	assign valid = (err == 0 && putStone != 0) ? 1 : 0;
endmodule// `include "input_detect.v"
// `include "player.v"
// `include "vga.v"
// `include "board_status.v"

module top ( // Judge
	input clk,
	input rst,
    output reg [15:0] LED,
    output [6:0] DISPLAY,
    output [3:0] DIGIT,
    inout PS2_DATA,
	inout PS2_CLK,
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue,
	output hsync,
	output vsync
);
	wire [2:0] putStone;
	wire en, valid;
	reg enabled, next_enabled;
	reg [0:83] board, next_board;
	wire [0:83] board_out;
	wire [1:0] win;

    // AI
    reg reqAI, next_reqAI;
    wire validAI;
    wire [0:83] board_AI;
    reg [4:0] timer, next_timer;
    reg isThinking, next_isThinking;

    // FSM
    reg [3:0] state, next_state;
    parameter INIT = 4'b0000;
    parameter TURN_HUMAN = 4'b0001;
    parameter TURN_AI = 4'b0010;
    parameter OUTCOME = 4'b0011;

    reg isWaiting, next_isWaiting;
    reg [1:0] turn, next_turn;

    reg [15:0] next_LED;

    wire clk_25M;

    Clock_divider #(4) c0(.clk_in(clk), .clk_out(clk_25M));

    inputDetect detector1 (PS2_DATA, PS2_CLK, clk_25M, rst, putStone, en);
    Player player1 (putStone, board, board_out, valid);
    vga vga1 (clk, rst, en, win, board, vgaRed, vgaGreen, vgaBlue, hsync, vsync);
    boardStatus status1 (board, win);

    Display d0(.clock(clk_25M), .turn(turn), .digit(DIGIT), .display(DISPLAY), .reset(rst));

    AIplayer a0(.clk(clk_25M), .rst(rst), .req(reqAI), .Board(board), .newBoard(board_AI), .valid(validAI));

    always @(posedge clk_25M or posedge rst) begin
        if (rst == 1) begin
            board = 84'b0;
            enabled = 0;
        end
        else begin
            board = next_board;
            enabled = next_enabled;
        end
    end

    always @ (posedge clk_25M, posedge rst) begin
        if (rst) begin
            state = INIT;
            reqAI = 0; 
            timer = 0;
            isThinking = 0;
            isWaiting = 0;
            turn = 0;
            LED = 16'h0000;
        end
        else begin
            state = next_state;
            reqAI = next_reqAI;
            timer = next_timer;
            isThinking = next_isThinking;
            isWaiting = next_isWaiting;
            turn = next_turn;
            LED = next_LED;
        end
    end

    always @ (*) begin
        next_enabled = enabled;
        next_board = board;



        next_state = state;
        next_reqAI = reqAI;
        next_timer = timer;
        next_isThinking = isThinking;
        next_isWaiting = isWaiting;
        next_turn = turn;
        next_LED = LED;

        case(state) 
            INIT: begin
                next_LED = 16'h000F;
                if (en == 1) begin
                    next_enabled = 1;
                    next_state = TURN_HUMAN;
                    next_isWaiting = 1;
                end
            end
            TURN_HUMAN: begin
                next_turn = 2'b01;
                next_LED = 16'h00F0;

                if (valid)begin 
                    next_board = board_out;
                    next_isWaiting = 0;
                end

                if (!isWaiting) begin 
                    if (win != 0) begin
                        next_state = OUTCOME;
                    end
                    else begin
                        next_state = TURN_AI;
                        next_reqAI = 1;
                        // next_timer = 31;
                        next_isThinking = 1;
                    end
                end
            end
            TURN_AI: begin
                next_turn = 2'b11;
                next_LED = 16'h0F00;

                next_reqAI = 0;

                if (validAI) begin
                    next_isThinking = 0;
                    next_board = board_AI;
                end

                if (!isThinking) begin
                    if (win != 0) begin
                        next_state = OUTCOME;
                    end
                    else begin
                        next_state = TURN_HUMAN;
                        next_isWaiting = 1;
                    end
                end

            end
            OUTCOME: begin
                next_LED = 16'hF000;
                next_turn = 0;
            end
            default: begin
                next_state = INIT;
            end
        endcase
    end

endmodule
	// AI_mod ai1 (req, board, board_AI, rst, clk, AI_valid);		// modify this
`define BLANK 2'd0
`define YELLOW 2'd3		// AI
`define RED 2'd1		// human

module vga (
	input clk,
	input rst,
	input en,
	input [1:0] win,
	input [0:83] board,		// comment out when testing
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue,
	output hsync,
	output vsync
);
	wire clk_25M, valid, enabled;
	wire [9:0] h_cnt, v_cnt;
	wire [11:0] signal;

	// wire [0:83] board = {{`RED},{`YELLOW},80'b0};		// comment out before release
	// wire [1:0] win = 2'b1;		// comment out before release

	assign {vgaRed, vgaGreen, vgaBlue} = (enabled == 1) ? signal : 12'h0;

	clk_divider divider1 (clk_25M, clk);
	enCtrl enctrl1 (en, clk, rst, enabled);
	vga_ctrl vga1 (hsync, vsync, valid, h_cnt, v_cnt, clk_25M, rst);
	pixelCtrl pixelctrl1 (valid, clk_25M, win, h_cnt, v_cnt, board, signal);

endmodule

module clk_divider (
	output clk_25M,
	input clk
);

	wire [22-1:0] next_num;
	reg [22-1:0] num;

	always @(posedge clk) begin
		num = next_num;
	end

	assign next_num = num + 1'b1;
	assign clk_25M = num[1];
endmodule

module enCtrl (
	input en,
	input clk,
	input rst,
	output reg enabled
);
	wire next_enabled = (en == 1) ? 1 : enabled;

	always @(posedge clk or posedge rst) begin
		if (rst == 1) enabled = 0;
		else enabled = next_enabled;
	end


endmodule

module vga_ctrl (
	output reg hsync, vsync,
	output valid,
	output [9:0] h_cnt, v_cnt,
	input clk,
	input rst
);

	reg [9:0] pixel_cnt;
	reg [9:0] line_cnt;

	// Horizontal signal
	parameter HORIZON_VISIBLE = 640;
	parameter HORIZON_BEF_PULSE = 656;		// 640 + 16
	parameter HORIZON_AFT_PULSE = 752;		// 640 + 16 + 96
	parameter HORIZON_TOTAL = 800;

	// Vertical signal
	parameter VERTICAL_VISIBLE = 480;
	parameter VERTICAL_BEF_PULSE = 490;		// 480 + 10
	parameter VERTICAL_AFT_PULSE = 492;		// 480 + 10 + 2
	parameter VERTICAL_TOTAL = 525;


	always @(posedge clk) begin		// pixel_cnt ctrl
		if (rst) begin
			pixel_cnt = 0;
		end else begin
			if (pixel_cnt == HORIZON_TOTAL - 1) begin
				pixel_cnt = 0;
			end else begin
				pixel_cnt = pixel_cnt + 1;
			end
		end
	end

	always @(posedge clk) begin		// hsync
		if (rst) begin
			hsync = 1;
		end else begin
			if ((pixel_cnt >= (HORIZON_BEF_PULSE - 1)) && (pixel_cnt < (HORIZON_AFT_PULSE - 1))) begin
				hsync = 0;
			end else begin
				hsync = 1;
			end
		end
	end

	always @(posedge clk) begin		// line_cnt ctrl
		if (rst) begin
			line_cnt = 0;
		end else begin
			if (pixel_cnt == HORIZON_TOTAL - 1) begin
				if (line_cnt == VERTICAL_TOTAL - 1) begin
					line_cnt = 0;
				end else begin
					line_cnt = line_cnt + 1;
				end
			end
		end
	end

	always @(posedge clk) begin		// vsync
		if (rst) begin
			vsync = 1;
		end else begin
			if ((line_cnt >= (VERTICAL_BEF_PULSE - 1)) && (line_cnt < (VERTICAL_AFT_PULSE - 1))) begin
				vsync = 0;
			end else begin
				vsync = 1;
			end
		end
	end

	assign valid = ((pixel_cnt < HORIZON_VISIBLE) && (line_cnt < VERTICAL_VISIBLE));
	assign h_cnt = (pixel_cnt < HORIZON_VISIBLE) ? pixel_cnt : 0;
	assign v_cnt = (line_cnt < VERTICAL_VISIBLE) ? line_cnt : 0;

endmodule

module pixelCtrl (
	input valid,
	input clk,
	input [1:0] win,
	input [9:0] h_cnt,
	input [9:0] v_cnt,
	input [0:83] board,
	output reg [11:0] signal
);

	parameter WHITE = 12'hfff;
	parameter BLUE = 12'h128;

	reg [10:0] stone_mem_addr;
	reg [12:0] win_msg_addr;
	wire [11:0] r_clr;
	wire [11:0] y_clr;
	wire [11:0] w_clr;
	wire [11:0] ai_win_msg_pxl, player_win_msg_pxl;

	// instantiate blk modules
    red_mem_blk redStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(r_clr), .wea(1'b0));
    white_mem_blk noStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(w_clr), .wea(1'b0));
    yellow_mem_blk yellowStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(y_clr), .wea(1'b0));
	aiwon_mem_blk AIWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(ai_win_msg_pxl), .wea(1'b0));
    pwon_mem_blk PlayerWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(player_win_msg_pxl), .wea(1'b0));

	always @(*) begin
		stone_mem_addr = 0;
		win_msg_addr = 0;
		if (h_cnt > 240 && h_cnt <= 400 && v_cnt >= 35 && v_cnt < 85) begin		// win message
			win_msg_addr = (h_cnt - 240) + (v_cnt - 35) * 160;
			case (win)
				2'b1: signal = player_win_msg_pxl;
				2'b10: signal = ai_win_msg_pxl;
				default: signal = BLUE;
			endcase
		end else if (h_cnt > 81 && h_cnt <= 121) begin
			// first column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (1,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 120) * 40;
				case (board[70:71])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (1,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 178) * 40;
				case (board[56:57])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (1,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 236) * 40;
				case (board[42:43])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (1,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 294) * 40;
				case (board[28:29])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (1,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 352) * 40;
				case (board[14:15])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (1,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 410) * 40;
				case (board[0:1])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 154 && h_cnt <= 194) begin
			// 2nd column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (2,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 120) * 40;
				case (board[72:73])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (2,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 178) * 40;
				case (board[58:59])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (2,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 236) * 40;
				case (board[44:45])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (2,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 294) * 40;
				case (board[30:31])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (2,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 352) * 40;
				case (board[16:17])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (2,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 410) * 40;
				case (board[2:3])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 227 && h_cnt <= 267) begin
			// 3rd column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (3,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 120) * 40;
				case (board[74:75])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (3,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 178) * 40;
				case (board[60:61])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (3,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 236) * 40;
				case (board[46:47])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (3,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 294) * 40;
				case (board[32:33])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (3,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 352) * 40;
				case (board[18:19])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (3,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 410) * 40;
				case (board[4:5])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 300 && h_cnt <= 340) begin
			// 4th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (4,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 120) * 40;
				case (board[76:77])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (4,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 178) * 40;
				case (board[62:63])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (4,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 236) * 40;
				case (board[48:49])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (4,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 294) * 40;
				case (board[34:35])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (4,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 352) * 40;
				case (board[20:21])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (4,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 410) * 40;
				case (board[6:7])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 373 && h_cnt <= 413) begin
			// 5th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (5,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 120) * 40;
				case (board[78:79])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (5,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 178) * 40;
				case (board[64:65])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (5,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 236) * 40;
				case (board[50:51])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (5,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 294) * 40;
				case (board[36:37])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (5,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 352) * 40;
				case (board[22:23])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (5,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 410) * 40;
				case (board[8:9])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 446 && h_cnt <= 486) begin
			// 6th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (6,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 120) * 40;
				case (board[80:81])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (6,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 178) * 40;
				case (board[66:67])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (6,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 236) * 40;
				case (board[52:53])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (6,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 294) * 40;
				case (board[38:39])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (6,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 352) * 40;
				case (board[24:25])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (6,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 410) * 40;
				case (board[10:11])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 519 && h_cnt <= 559) begin
			// 7th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (7,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 120) * 40;
				case (board[82:83])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (7,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 178) * 40;
				case (board[68:69])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (7,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 236) * 40;
				case (board[54:55])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (7,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 294) * 40;
				case (board[40:41])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (7,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 352) * 40;
				case (board[26:27])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (7,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 410) * 40;
				case (board[12:13])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (valid == 1) signal = BLUE;
		else signal = 0;
	end
endmodule
