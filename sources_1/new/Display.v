module Display (clock, turn, digit, display, reset);
    input clock, reset;
    input [1:0] turn;
    output reg [3:0] digit;
    output reg [6:0] display;

    reg [3:0] bcd0, bcd1, bcd2, bcd3;
    reg [3:0] value;

// module Clock_divider (clk_in, clk_out);
    
    // HM / AI
    // 43   21
    always @ (*) begin
            bcd0 = (turn == 2'b11) ? 1: 0;
            bcd1 = (turn == 2'b11) ? 2: 0;
            bcd2 = (turn == 2'b01) ? 3: 0; 
            bcd3 = (turn == 2'b01) ? 4: 0;
    end

    always @ (posedge clock, posedge reset) begin
        if (reset == 1) begin
            digit = 4'b0000;
            value = 0;
        end
        else begin
            case(digit)  
                4'b1110: begin
                    value = bcd1;
                    digit = 4'b1101;
                end
                4'b1101: begin
                    value = bcd2;
                    digit = 4'b1011;
                end
                4'b1011: begin
                    value = bcd3;
                    digit = 4'b0111;
                end
                4'b0111: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
                default: begin
                    value = bcd0;
                    digit = 4'b1110;
                end
            endcase
        end
    end

    always @ (*) begin
        case(value)
            4'd0: display = 7'b1111111;// nothing 
            4'd1: display = 7'b1001111; //I
            4'd2: display = 7'b0001000; // A
            4'd3: display = 7'b1101010; // M 
            4'd4: display = 7'b0001001; // H
            default: display = 7'b1111111;
        endcase
    end

endmodule

