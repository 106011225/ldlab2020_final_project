module ledCtrl (		// no bug
	input clk,
	input rst,
	input [1:0] win,
	output reg [15:0] led
);
	reg [15:0] next_led;

	always @(posedge clk or posedge rst) begin
		if (rst == 1) led = 16'b0;
		else led = next_led;
	end

	always @(*) begin
		next_led = led;
		case (win)
			1: begin		// player win
				if (led[7] == 0) begin
					next_led[7:1] = led[6:0];
					next_led[0] = 1'b1;
				end
			end
			2: begin
				if (led[8] == 0) begin
					next_led[14:8] = led[15:9];
					next_led[15] = 1'b1;
				end
			end
		endcase
	end
endmodule