`define BLANK 2'd0
`define YELLOW 2'd3		// AI
`define RED 2'd1		// human

module vga (
	input clk,
	input rst,
	input en,
	input mode,
	input [1:0] win,
	input [0:83] board,		// comment out when testing
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue,
	output hsync,
	output vsync
);
	wire clk_25M, valid, enabled;
	wire [9:0] h_cnt, v_cnt;
	wire [11:0] signal, start_menu_signal;
	wire [11:0] start_screen;

	// wire [0:83] board = {{`RED},{`YELLOW},80'b0};		// comment out before release
	// wire [1:0] win = 2'b1;		// comment out before release

	assign {vgaRed, vgaGreen, vgaBlue} = (enabled == 1) ? signal : start_menu_signal;

	clk_divider divider1 (clk_25M, clk);
	enCtrl enctrl1 (en, clk, rst, enabled);
	vga_ctrl vga1 (hsync, vsync, valid, h_cnt, v_cnt, clk_25M, rst);
	pixelCtrl pixelctrl1 (valid, clk_25M, mode, win, h_cnt, v_cnt, board, signal, start_menu_signal);

endmodule

module clk_divider (
	output clk_25M,
	input clk
);

	wire [22-1:0] next_num;
	reg [22-1:0] num;

	always @(posedge clk) begin
		num = next_num;
	end

	assign next_num = num + 1'b1;
	assign clk_25M = num[1];
endmodule

module enCtrl (
	input en,
	input clk,
	input rst,
	output reg enabled
);
	wire next_enabled = (en == 1) ? 1 : enabled;

	always @(posedge clk or posedge rst) begin
		if (rst == 1) enabled = 0;
		else enabled = next_enabled;
	end


endmodule

module vga_ctrl (
	output reg hsync, vsync,
	output valid,
	output [9:0] h_cnt, v_cnt,
	input clk,
	input rst
);

	reg [9:0] pixel_cnt;
	reg [9:0] line_cnt;

	// Horizontal signal
	parameter HORIZON_VISIBLE = 640;
	parameter HORIZON_BEF_PULSE = 656;		// 640 + 16
	parameter HORIZON_AFT_PULSE = 752;		// 640 + 16 + 96
	parameter HORIZON_TOTAL = 800;

	// Vertical signal
	parameter VERTICAL_VISIBLE = 480;
	parameter VERTICAL_BEF_PULSE = 490;		// 480 + 10
	parameter VERTICAL_AFT_PULSE = 492;		// 480 + 10 + 2
	parameter VERTICAL_TOTAL = 525;


	always @(posedge clk) begin		// pixel_cnt ctrl
		if (rst) begin
			pixel_cnt = 0;
		end else begin
			if (pixel_cnt == HORIZON_TOTAL - 1) begin
				pixel_cnt = 0;
			end else begin
				pixel_cnt = pixel_cnt + 1;
			end
		end
	end

	always @(posedge clk) begin		// hsync
		if (rst) begin
			hsync = 1;
		end else begin
			if ((pixel_cnt >= (HORIZON_BEF_PULSE - 1)) && (pixel_cnt < (HORIZON_AFT_PULSE - 1))) begin
				hsync = 0;
			end else begin
				hsync = 1;
			end
		end
	end

	always @(posedge clk) begin		// line_cnt ctrl
		if (rst) begin
			line_cnt = 0;
		end else begin
			if (pixel_cnt == HORIZON_TOTAL - 1) begin
				if (line_cnt == VERTICAL_TOTAL - 1) begin
					line_cnt = 0;
				end else begin
					line_cnt = line_cnt + 1;
				end
			end
		end
	end

	always @(posedge clk) begin		// vsync
		if (rst) begin
			vsync = 1;
		end else begin
			if ((line_cnt >= (VERTICAL_BEF_PULSE - 1)) && (line_cnt < (VERTICAL_AFT_PULSE - 1))) begin
				vsync = 0;
			end else begin
				vsync = 1;
			end
		end
	end

	assign valid = ((pixel_cnt < HORIZON_VISIBLE) && (line_cnt < VERTICAL_VISIBLE));
	assign h_cnt = (pixel_cnt < HORIZON_VISIBLE) ? pixel_cnt : 0;
	assign v_cnt = (line_cnt < VERTICAL_VISIBLE) ? line_cnt : 0;

endmodule

module pixelCtrl (
	input valid,
	input clk,
	input mode,
	input [1:0] win,
	input [9:0] h_cnt,
	input [9:0] v_cnt,
	input [0:83] board,
	output reg [11:0] signal,
	output reg [11:0] start_menu_signal
);

	parameter WHITE = 12'hfff;
	parameter BLUE = 12'h128;

	reg [10:0] stone_mem_addr;
	reg [12:0] win_msg_addr;
	reg [15:0] start_menu_addr;
	wire [11:0] r_clr;
	wire [11:0] y_clr;
	wire [11:0] w_clr;
	wire [11:0] ai_win_msg_pxl, player_win_msg_pxl, onep_win_msg, twop_win_msg, draw_msg;
	wire [11:0] start_menu_pxl;

	// instantiate blk modules
	red_mem_blk redStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(r_clr), .wea(1'b0));
	white_mem_blk noStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(w_clr), .wea(1'b0));
	yellow_mem_blk yellowStone (.addra(stone_mem_addr), .clka(clk), .dina(12'b0), .douta(y_clr), .wea(1'b0));
	aiwon_mem_blk AIWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(ai_win_msg_pxl), .wea(1'b0));
	pwon_mem_blk PlayerWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(player_win_msg_pxl), .wea(1'b0));
	onepwon_mem_blk onePWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(onep_win_msg), .wea(1'b0));
	twopwon_mem_blk twoPWinMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(twop_win_msg), .wea(1'b0));
	draw_mem_blk drawMsg (.addra(win_msg_addr), .clka(clk), .dina(12'b0), .douta(draw_msg), .wea(1'b0));
	start_mem_blk StartMenu (.addra(start_menu_addr), .clka(clk), .dina(12'b0), .douta(start_menu_pxl), .wea(1'b0));

	always @(*) begin
		start_menu_addr = 0;
		if (h_cnt > 170 && h_cnt <= 470 && v_cnt >= 140 && v_cnt < 300) begin
			start_menu_addr = (h_cnt - 170) + (v_cnt - 140) * 300;
			start_menu_signal = start_menu_pxl;
		end else start_menu_signal = BLUE;
	end

	always @(*) begin
		stone_mem_addr = 0;
		win_msg_addr = 0;
		if (h_cnt > 240 && h_cnt <= 400 && v_cnt >= 35 && v_cnt < 85) begin		// win message
			win_msg_addr = (h_cnt - 240) + (v_cnt - 35) * 160;
			case ({mode, win})
				3'b1: signal = player_win_msg_pxl;
				3'b10: signal = ai_win_msg_pxl;
				3'b101: signal = onep_win_msg;
				3'b110: signal = twop_win_msg;
				3'b11: signal = draw_msg;
				3'b111: signal = draw_msg;
				default: signal = BLUE;
			endcase
		end else if (h_cnt > 81 && h_cnt <= 121) begin
			// first column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (1,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 120) * 40;
				case (board[70:71])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (1,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 178) * 40;
				case (board[56:57])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (1,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 236) * 40;
				case (board[42:43])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (1,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 294) * 40;
				case (board[28:29])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (1,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 352) * 40;
				case (board[14:15])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (1,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 81) + (v_cnt - 410) * 40;
				case (board[0:1])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 154 && h_cnt <= 194) begin
			// 2nd column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (2,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 120) * 40;
				case (board[72:73])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (2,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 178) * 40;
				case (board[58:59])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (2,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 236) * 40;
				case (board[44:45])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (2,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 294) * 40;
				case (board[30:31])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (2,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 352) * 40;
				case (board[16:17])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (2,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 154) + (v_cnt - 410) * 40;
				case (board[2:3])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 227 && h_cnt <= 267) begin
			// 3rd column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (3,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 120) * 40;
				case (board[74:75])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (3,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 178) * 40;
				case (board[60:61])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (3,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 236) * 40;
				case (board[46:47])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (3,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 294) * 40;
				case (board[32:33])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (3,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 352) * 40;
				case (board[18:19])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (3,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 227) + (v_cnt - 410) * 40;
				case (board[4:5])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 300 && h_cnt <= 340) begin
			// 4th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (4,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 120) * 40;
				case (board[76:77])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (4,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 178) * 40;
				case (board[62:63])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (4,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 236) * 40;
				case (board[48:49])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (4,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 294) * 40;
				case (board[34:35])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (4,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 352) * 40;
				case (board[20:21])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (4,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 300) + (v_cnt - 410) * 40;
				case (board[6:7])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 373 && h_cnt <= 413) begin
			// 5th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (5,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 120) * 40;
				case (board[78:79])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (5,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 178) * 40;
				case (board[64:65])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (5,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 236) * 40;
				case (board[50:51])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (5,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 294) * 40;
				case (board[36:37])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (5,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 352) * 40;
				case (board[22:23])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (5,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 373) + (v_cnt - 410) * 40;
				case (board[8:9])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 446 && h_cnt <= 486) begin
			// 6th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (6,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 120) * 40;
				case (board[80:81])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (6,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 178) * 40;
				case (board[66:67])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (6,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 236) * 40;
				case (board[52:53])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (6,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 294) * 40;
				case (board[38:39])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (6,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 352) * 40;
				case (board[24:25])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (6,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 446) + (v_cnt - 410) * 40;
				case (board[10:11])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (h_cnt > 519 && h_cnt <= 559) begin
			// 7th column
			if (v_cnt >= 120 && v_cnt < 160) begin
				// (7,1)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 120) * 40;
				case (board[82:83])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 178 && v_cnt < 218) begin
				// (7,2)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 178) * 40;
				case (board[68:69])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 236 && v_cnt < 276) begin
				// (7,3)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 236) * 40;
				case (board[54:55])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 294 && v_cnt < 334) begin
				// (7,4)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 294) * 40;
				case (board[40:41])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 352 && v_cnt < 392) begin
				// (7,5)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 352) * 40;
				case (board[26:27])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else if (v_cnt >= 410 && v_cnt < 450) begin
				// (7,6)
				// signal = WHITE;
				stone_mem_addr = (h_cnt - 519) + (v_cnt - 410) * 40;
				case (board[12:13])
					`BLANK: signal = w_clr;
					`YELLOW: signal = y_clr;
					`RED: signal = r_clr;
					default: signal = WHITE;
				endcase
			end else signal = BLUE;
		end else if (valid == 1) signal = BLUE;
		else signal = 0;
	end
endmodule