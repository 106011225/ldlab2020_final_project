module Clock_divider (clk_in, clk_out);
    parameter n = 25;
    input clk_in;
    output clk_out;
    reg [n-1:0] num = 0;
    wire [n-1:0] next_num;

    always @ (posedge clk_in) 
        num = next_num;

    assign next_num = num + 1;
    assign clk_out = num[n-1];
endmodule
