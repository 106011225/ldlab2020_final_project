`define BLANK 2'd0
`define YELLOW 2'd3		// AI
`define RED 2'd1		// human

module boardStatus (
	input [0:83] board,
	output [1:0] win
);
	// 0: no one win, 1: human win, 2: AI win
	parameter HUMAN = 1;
	parameter AI = 2;
	parameter draw = 3;

	integer i;
	integer j;

	reg [1:0] win_i;
	reg draw_flag;

	always @(*) begin		// detect win condition
		win_i = 0;
		draw_flag = 1;

		for (i = 70; i >= 0; i = i - 14) begin		// horizontal condition
			for (j = i; j <= (i+6); j = j+2) begin	// col 1 ~ 4
				if (board[j+:2] == `YELLOW && board[(j+2)+:2] == `YELLOW && board[(j+4)+:2] == `YELLOW && board[(j+6)+:2] == `YELLOW) win_i = AI;
				else if (board[j+:2] == `RED && board[(j+2)+:2] == `RED && board[(j+4)+:2] == `RED && board[(j+6)+:2] == `RED) win_i = HUMAN;
			end
		end

		for (i = 70; i >= 42; i = i - 14) begin		// veritcal condition
			for (j = i; j <= (i+12); j = j+2) begin	// row 1 ~ 3
				if (board[j+:2] == `YELLOW && board[(j-14)+:2] == `YELLOW && board[(j-28)+:2] == `YELLOW && board[(j-42)+:2] == `YELLOW) win_i = AI;
				else if (board[j+:2] == `RED && board[(j-14)+:2] == `RED && board[(j-28)+:2] == `RED && board[(j-42)+:2] == `RED) win_i = HUMAN;
			end
		end

		for (i = 70; i >= 42; i = i - 14) begin		// 左上右下
			for (j = i; j <= (i+6); j = j+2) begin	// (1,1) ~ (4,3)
				if (board[j+:2] == `YELLOW && board[(j-12)+:2] == `YELLOW && board[(j-24)+:2] == `YELLOW && board[(j-36)+:2] == `YELLOW) win_i = AI;
				else if (board[j+:2] == `RED && board[(j-12)+:2] == `RED && board[(j-24)+:2] == `RED && board[(j-36)+:2] == `RED) win_i = HUMAN;
			end
		end

		for (i = 76; i >= 48; i = i - 14) begin		// 右上左下
			for (j = i; j <= (i+6); j = j+2) begin	// (4,1) ~ (7,3)
				if (board[j+:2] == `YELLOW && board[(j-16)+:2] == `YELLOW && board[(j-32)+:2] == `YELLOW && board[(j-48)+:2] == `YELLOW) win_i = AI;
				else if (board[j+:2] == `RED && board[(j-16)+:2] == `RED && board[(j-32)+:2] == `RED && board[(j-48)+:2] == `RED) win_i = HUMAN;
			end
		end

		for (i = 0; i <= 82;i = i + 1) begin
			if (board[i+:2] == `BLANK) draw_flag = 0;
		end
	end

	assign win = (draw_flag == 1) ? draw : win_i;
endmodule