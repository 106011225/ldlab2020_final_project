`include "input_detect.v"
`include "player.v"
`include "vga.v"
`include "board_status.v"

module top ( // Judge
	input clk,
	input rst,
	// input mode,
	// output [15:0] LED,
	// output [6:0] DISPLAY,
	// output [3:0] DIGIT,
	inout PS2_DATA,
	inout PS2_CLK,
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue,
	output hsync,
	output vsync
);
	wire [2:0] putStone;
	wire en, valid1, valid2;
	reg enabled, next_enabled;
	reg [0:83] board, next_board;
	wire [0:83] board_1, board_2;
	wire [1:0] win;
	reg turn, next_turn;

	wire mode = 1;
	wire [2:0] putStone_player = (win == 0) ? putStone : 0;

	inputDetect detector1 (PS2_DATA, PS2_CLK, clk, rst, putStone, en);
	Player player1 (putStone_player, 1'b0, board, board_1, valid1);
	Player player2 (putStone_player, 1'b1, board, board_2, valid2);
	vga vga1 (clk, rst, en, mode, win, board, vgaRed, vgaGreen, vgaBlue, hsync, vsync);
	boardStatus status1 (board, win);

	always @(posedge clk or posedge rst) begin
		if (rst == 1) begin
			board = 84'b0;
			enabled = 0;
			turn = 0;
		end
		else begin
			board = next_board;
			enabled = next_enabled;
			turn = next_turn;
		end
	end

	always @(*) begin
		next_enabled = enabled;
		next_board = board;
		next_turn = turn;
		if (en == 1) next_enabled = 1;
		if (enabled == 1) begin
			// if (mode == 1) begin		// mode selection
			case (turn)
				0: begin
					if (valid1 == 1) begin
						next_board = board_1;
						next_turn = 1'b1;
					end
				end
				1: begin
					if (valid2 == 1) begin
						next_board = board_2;
						next_turn = 1'b0;
					end
				end
			endcase
			// end						// end mode selection
		end
	end
endmodule
	// AI_mod ai1 (req, board, board_AI, rst, clk, AI_valid);		// modify this
	// Player player1 (.putStone(putStone), .board_i(board), .board_o(board_player), .valid(player_valid));
	// inputDetect detector1 (.PS2_DATA(PS2_DATA), .PS2_CLK(PS2_CLK), .clk(clk), .rst(rst), .putStone(putStone), .en(en));		// clk and rst is for keyboard ctrl
	// boardStatus status1 (.board(board), .win(win));
	// vga vga1 (.clk(clk), .rst(rst), .en(en), .board(board), .vgaRed(vgaRed), vgaGreen(vgaGreen), vgaBlue(vgaBlue), .hsync(hsync), .vsync(vsync));	// no need clock divider, already instantiated.
