module Onepulse(out, in, clock);
    input in, clock;
    output reg out;

    reg in_delayn;

    always @ (posedge clock) begin
        in_delayn <= ~in;

        if (in & in_delayn)
            out <= 1;
        else 
            out <= 0;
    end
endmodule
