`define BLANK 2'd0
`define RED 2'd1		// player 1
`define YELLOW 2'd3		// AI or 2P

module Player (
	input [2:0] putStone,
	input playerNum,
	input [0:83] board_i,
	output reg [0:83] board_o,
	output valid
);
	integer i;

	reg err, placed;

	always @(*) begin
		err = 0;
		placed = 0;
		board_o = board_i;
		case (putStone)
			1: begin
				for (i = 0; i <= 70; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 70) err = 1;
				end
			end
			2: begin
				for (i = 2; i <= 72; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 72) err = 1;
				end
			end
			3: begin
				for (i = 4; i <= 74; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 74) err = 1;
				end
			end
			4: begin
				for (i = 6; i <= 76; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 76) err = 1;
				end
			end
			5: begin
				for (i = 8; i <= 78; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 78) err = 1;
				end
			end
			6: begin
				for (i = 10; i <= 80; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 80) err = 1;
				end
			end
			7: begin
				for (i = 12; i <= 82; i = i+14) begin
					if (board_o[i+:2] == `BLANK && placed == 0) begin
						board_o[i+:2] = (playerNum == 0) ? `RED : `YELLOW;
						placed = 1;
					end else if (placed == 0 && i == 82) err = 1;
				end
			end
		endcase
	end

	assign valid = (err == 0 && putStone != 0) ? 1 : 0;
endmodule