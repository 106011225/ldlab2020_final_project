module inputDetect (		// no problem
	inout PS2_DATA,
	inout PS2_CLK,
	input clk,
	input rst,
    output reg [2:0] putStone,
	output reg en
);

	wire [7:0] key_in;
	wire is_extend, is_break, valid, err;

	KeyboardCtrl_0 inst (
		.key_in(key_in),
		.is_extend(is_extend),
		.is_break(is_break),
		.valid(valid),
		.err(err),
		.PS2_DATA(PS2_DATA),
		.PS2_CLK(PS2_CLK),
		.rst(rst),
		.clk(clk)
	);

    always @(*) begin
		putStone = 0;
		en = 0;
        if (is_extend == 0 && is_break == 0 && valid == 1) begin
			case (key_in)
				8'h16: putStone = 1;
				8'h1E: putStone = 2;
				8'h26: putStone = 3;
				8'h25: putStone = 4;
				8'h2E: putStone = 5;
				8'h36: putStone = 6;
				8'h3D: putStone = 7;
				8'h5A: en = 1;
				default: begin
					putStone = 0;
					en = 0;
				end
			endcase
		end
    end
endmodule