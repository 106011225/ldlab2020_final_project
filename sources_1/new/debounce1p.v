module debounce1p #(parameter shamt = 10) (
	input clk,
	input bef,
	output aft
);
	reg [shamt:0] shift_reg;

	always @(posedge clk) begin
		shift_reg [shamt:1] = shift_reg[shamt-1:0];
		shift_reg [0] = bef;
	end

	assign aft = (shift_reg == {1'b0, {shamt{1'b1}}}) ? 1 : 0;
endmodule