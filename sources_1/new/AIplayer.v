// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01

// define bit width
`define NUM_PARAMS_BITS 25 

module AIplayer(
    input wire clk,
    input wire rst,
    input wire req,
    input wire [0 : 2*`BOARD_H*`BOARD_W-1] Board,
    output reg [0 : 2*`BOARD_H*`BOARD_W-1] newBoard,
    output reg valid
);


    // FSM
    parameter INIT = 3'b000;
    parameter CHECK_CRITICAL_MOVE = 3'b001;
    parameter FORWARD_NN = 3'b010;
    parameter NN_MOVE = 3'b011;
    parameter OUTP = 3'b111;
    reg [2:0] state, next_state;

    // flag
    reg isCritical, next_isCritical;//

    // output
    reg [0 : 2*`BOARD_H*`BOARD_W-1] next_newBoard;
    reg next_valid;
    

    // NN
    reg [2:0] testingCol, next_testingCol;
    reg isProcessing, next_isProcessing;
    reg [`NUM_PARAMS_BITS-1 : 0] currMax, next_currMax;
    reg [2:0] currOptCol, next_currOptCol;
    reg [0 : 2*`BOARD_H*`BOARD_W-1] NNinputBoard, next_NNinputBoard;
    reg reqNN, next_reqNN;
    wire [`NUM_PARAMS_BITS-1 : 0] y;
    wire validNN;
    reg [0:3*7-1] colHeight, next_colHeight;

    integer i;
    NN n0(.clk(clk), .rst(rst), .req(reqNN), .Board(NNinputBoard), .y(y), .valid_1p(validNN));
            
    


    // DFF
    always @ (posedge clk, posedge rst) begin
        if (rst) begin
            state = INIT;
            newBoard = 84'b0;
            valid = 0;
            isCritical = 0;
            testingCol = 0;
            isProcessing = 0;
            currMax = 0;
            currOptCol = 0;
            NNinputBoard = 0;
            reqNN = 0;
            colHeight = 0;
        end
        else begin
            state = next_state;
            newBoard = next_newBoard;
            valid = next_valid;
            isCritical = next_isCritical;
            testingCol = next_testingCol;
            isProcessing = next_isProcessing;
            currMax = next_currMax;
            currOptCol = next_currOptCol;;
            NNinputBoard = next_NNinputBoard;
            reqNN = next_reqNN;
            colHeight = next_colHeight;
        end 
    end


    // combinational
    always @ (*) begin 
        next_state = state;
        next_newBoard = newBoard;
        next_valid = valid;
        next_isCritical = isCritical;
        next_testingCol = testingCol;
        next_isProcessing = isProcessing;
        next_currMax = currMax;
        next_currOptCol = currOptCol;
        next_NNinputBoard = NNinputBoard;
        next_reqNN = reqNN;
        next_colHeight = colHeight;

        case(state) 
            INIT: begin
                if (req) begin
                    next_state = CHECK_CRITICAL_MOVE;
                end
            end
            CHECK_CRITICAL_MOVE: begin

                next_state = FORWARD_NN; // by default
                next_testingCol = 3'b111; // -1
                next_isProcessing = 0;
                next_currMax = 0;
                next_currOptCol = 0;
                next_reqNN = 0;
                for (i = 0; i < 7; i=i+1) begin
                    next_colHeight[i*3 +: 3] = Board[1+i*2] + Board[15+i*2] + Board[29+i*2] + Board[43+i*2] + Board[57+i*2] + Board[71+i*2];
                end

                next_newBoard = Board;
                Task_checkCritical;
                /* if isCritical:
                    next_newBoard[...] = `STONE_AI___;
                    next_isCritical = 1;
                    next_state = OUTP;
                    next_valid = 1;
                */
                            
            end
            FORWARD_NN: begin

                // forward pass
                if (isProcessing) begin
                    if (validNN) begin // done of calculating
                        next_reqNN = 0;
                        next_isProcessing = 0;

                        if ((y[`NUM_PARAMS_BITS-1] == currMax[`NUM_PARAMS_BITS-1]) && (y > currMax) ) begin
                            next_currMax = y;
                            next_currOptCol = testingCol;
                        end
                        else if ((y[`NUM_PARAMS_BITS-1] == 0) && (currMax[`NUM_PARAMS_BITS-1]==1)) begin
                            next_currMax = y;
                            next_currOptCol = testingCol;
                        end
                    end
                end
                else begin // not isProcessing
                    // test another col 
                    if (testingCol == 3'b111 || testingCol < 6) begin
                        next_testingCol =  testingCol + 1;

                        if (colHeight[testingCol+1] < 6) begin
                            next_NNinputBoard = Board;
                            next_NNinputBoard[((colHeight[testingCol+1])*7 + (testingCol+1))*2 +: 2] = `STONE_AI___;
                            next_reqNN = 1;
                            next_isProcessing = 1;
                        end
                    end
                    // done, trans to next state
                    else if (testingCol == 6) begin
                        next_state = NN_MOVE;
                    end
                end
                
            end
            NN_MOVE: begin
                next_newBoard = Board;
                next_newBoard[((colHeight[currOptCol])*7 + (currOptCol)) +: 2] = `STONE_AI___;
                next_state = OUTP;
                next_valid = 1;
            end
            OUTP: begin
                next_state = INIT;
                next_valid = 0;

            end
            default: begin
                next_state = INIT;
            end
        endcase
    end

//end of module

    task Task_checkCritical;
    begin
            
        // vertical
        if((Board[42 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[14 +: 2] && Board[14 +: 2] == Board[28 +: 2])) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[48 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[34 +: 2])) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 0 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 0 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 2 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 4 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 2 +: 2] && Board[ 2 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[ 2 +: 2] && Board[ 2 +: 2] == Board[ 4 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[14 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 0 +: 2] != 0)) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[20 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[18 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[14 +: 2] != 0)) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 0 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2])) begin
            next_newBoard[ 0 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[48 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[ 0 +: 2] != 0) && (Board[ 0 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[32 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[ 6 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[42 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[42 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[42 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[30 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[44 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[30 +: 2])) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[50 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[36 +: 2])) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 2 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 4 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[ 4 +: 2] && Board[ 4 +: 2] == Board[ 6 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[ 2 +: 2] != 0)) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[22 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 2 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2])) begin
            next_newBoard[ 2 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[50 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[ 2 +: 2] != 0) && (Board[ 2 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[34 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[ 8 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[44 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[32 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[46 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[32 +: 2])) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[52 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[38 +: 2])) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 4 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 6 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[10 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[10 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[ 6 +: 2] && Board[ 6 +: 2] == Board[ 8 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[ 4 +: 2] != 0)) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[24 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 4 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2])) begin
            next_newBoard[ 4 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[52 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[ 4 +: 2] != 0) && (Board[ 4 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[36 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[10 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[46 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[10 +: 2] != 0) && (Board[10 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[34 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[48 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[34 +: 2])) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[54 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[26 +: 2] && Board[26 +: 2] == Board[40 +: 2])) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[ 6 +: 2] == 0) && (Board[ 8 +: 2] != 0) && (Board[ 8 +: 2] == Board[10 +: 2] && Board[10 +: 2] == Board[12 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[ 8 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[10 +: 2] && Board[10 +: 2] == Board[12 +: 2])) begin
            next_newBoard[ 8 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[10 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[12 +: 2])) begin
            next_newBoard[10 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[12 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[ 8 +: 2] && Board[ 8 +: 2] == Board[10 +: 2])) begin
            next_newBoard[12 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2]) && (Board[ 6 +: 2] != 0)) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[26 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[26 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2]) && (Board[12 +: 2] != 0)) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[26 +: 2] != 0)) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[ 6 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[54 +: 2])) begin
            next_newBoard[ 6 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[54 +: 2]) && (Board[ 8 +: 2] != 0)) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[54 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[ 6 +: 2] != 0) && (Board[ 6 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[38 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[12 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2])) begin
            next_newBoard[12 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2]) && (Board[10 +: 2] != 0)) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[48 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[12 +: 2] != 0) && (Board[12 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[36 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[56 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[28 +: 2] && Board[28 +: 2] == Board[42 +: 2])) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[62 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[48 +: 2])) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[14 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[16 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[20 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[16 +: 2] && Board[16 +: 2] == Board[18 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[14 +: 2] != 0)) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[62 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[60 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[14 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2])) begin
            next_newBoard[14 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[62 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[14 +: 2] != 0) && (Board[14 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[46 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[20 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[56 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[56 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[56 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[44 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[58 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[44 +: 2])) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[64 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[50 +: 2])) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[16 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[18 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[22 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[18 +: 2] && Board[18 +: 2] == Board[20 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[16 +: 2] != 0)) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[64 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[16 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2])) begin
            next_newBoard[16 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[64 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[16 +: 2] != 0) && (Board[16 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[48 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[22 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[58 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[46 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[60 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[46 +: 2])) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[66 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[52 +: 2])) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[18 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[20 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[24 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[20 +: 2] && Board[20 +: 2] == Board[22 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[18 +: 2] != 0)) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[66 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[18 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2])) begin
            next_newBoard[18 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[66 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[18 +: 2] != 0) && (Board[18 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[50 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[24 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[60 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[24 +: 2] != 0) && (Board[24 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[48 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[62 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[48 +: 2])) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[68 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[40 +: 2] && Board[40 +: 2] == Board[54 +: 2])) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[20 +: 2] == 0) && (Board[22 +: 2] != 0) && (Board[22 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[22 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[24 +: 2] && Board[24 +: 2] == Board[26 +: 2])) begin
            next_newBoard[22 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[24 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[26 +: 2])) begin
            next_newBoard[24 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[26 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[22 +: 2] && Board[22 +: 2] == Board[24 +: 2])) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[20 +: 2] != 0)) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2]) && (Board[26 +: 2] != 0)) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[64 +: 2] != 0) && (Board[64 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[68 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[20 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[68 +: 2])) begin
            next_newBoard[20 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[68 +: 2]) && (Board[22 +: 2] != 0)) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[68 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[20 +: 2] != 0) && (Board[20 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[52 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[26 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2])) begin
            next_newBoard[26 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2]) && (Board[24 +: 2] != 0)) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[62 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[26 +: 2] != 0) && (Board[26 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[50 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[70 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[42 +: 2] && Board[42 +: 2] == Board[56 +: 2])) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[76 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[62 +: 2])) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[28 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[30 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[34 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[30 +: 2] && Board[30 +: 2] == Board[32 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[42 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[28 +: 2] != 0)) begin
            next_newBoard[42 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[48 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[42 +: 2] != 0) && (Board[42 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[46 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[56 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[42 +: 2] != 0)) begin
            next_newBoard[56 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[62 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[56 +: 2] != 0) && (Board[56 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[60 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[70 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[56 +: 2] != 0)) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[72 +: 2] && Board[72 +: 2] == Board[76 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[70 +: 2] != 0) && (Board[70 +: 2] == Board[72 +: 2] && Board[72 +: 2] == Board[74 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[28 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[76 +: 2])) begin
            next_newBoard[28 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[76 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[76 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[28 +: 2] != 0) && (Board[28 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[60 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[34 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[70 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[58 +: 2] && Board[58 +: 2] == Board[70 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[70 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[70 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[58 +: 2]) && (Board[56 +: 2] != 0)) begin
            next_newBoard[70 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[72 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[44 +: 2] && Board[44 +: 2] == Board[58 +: 2])) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[78 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[64 +: 2])) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[30 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[32 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[36 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[32 +: 2] && Board[32 +: 2] == Board[34 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[44 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[30 +: 2] != 0)) begin
            next_newBoard[44 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[50 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[44 +: 2] != 0) && (Board[44 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[48 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[58 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[44 +: 2] != 0)) begin
            next_newBoard[58 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[64 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[58 +: 2] != 0) && (Board[58 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[62 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[78 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[72 +: 2] != 0) && (Board[72 +: 2] == Board[74 +: 2] && Board[74 +: 2] == Board[76 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[30 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[78 +: 2])) begin
            next_newBoard[30 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[78 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[78 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[30 +: 2] != 0) && (Board[30 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[62 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[36 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[72 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[60 +: 2] && Board[60 +: 2] == Board[72 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[72 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[72 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[60 +: 2]) && (Board[58 +: 2] != 0)) begin
            next_newBoard[72 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[74 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[46 +: 2] && Board[46 +: 2] == Board[60 +: 2])) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[80 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[66 +: 2])) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[32 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[34 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[38 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[34 +: 2] && Board[34 +: 2] == Board[36 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[46 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[32 +: 2] != 0)) begin
            next_newBoard[46 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[52 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[46 +: 2] != 0) && (Board[46 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[50 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[60 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[46 +: 2] != 0)) begin
            next_newBoard[60 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[66 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[60 +: 2] != 0) && (Board[60 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[64 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[80 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[74 +: 2] != 0) && (Board[74 +: 2] == Board[76 +: 2] && Board[76 +: 2] == Board[78 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[32 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[80 +: 2])) begin
            next_newBoard[32 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[80 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[80 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[32 +: 2] != 0) && (Board[32 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[64 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[38 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[74 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[62 +: 2] && Board[62 +: 2] == Board[74 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[74 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[74 +: 2] == 0) && (Board[38 +: 2] != 0) && (Board[38 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[62 +: 2]) && (Board[60 +: 2] != 0)) begin
            next_newBoard[74 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // vertical
        if((Board[76 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[48 +: 2] && Board[48 +: 2] == Board[62 +: 2])) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        if((Board[82 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[54 +: 2] && Board[54 +: 2] == Board[68 +: 2])) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // horizontal
        else if((Board[34 +: 2] == 0) && (Board[36 +: 2] != 0) && (Board[36 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[36 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[38 +: 2] && Board[38 +: 2] == Board[40 +: 2])) begin
            next_newBoard[36 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[38 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[40 +: 2])) begin
            next_newBoard[38 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[40 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[36 +: 2] && Board[36 +: 2] == Board[38 +: 2])) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[48 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[34 +: 2] != 0)) begin
            next_newBoard[48 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[54 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[54 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[54 +: 2] == 0) && (Board[48 +: 2] != 0) && (Board[48 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[52 +: 2]) && (Board[40 +: 2] != 0)) begin
            next_newBoard[54 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[62 +: 2] == 0) && (Board[64 +: 2] != 0) && (Board[64 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[48 +: 2] != 0)) begin
            next_newBoard[62 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[68 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[68 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[68 +: 2] == 0) && (Board[62 +: 2] != 0) && (Board[62 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[66 +: 2]) && (Board[54 +: 2] != 0)) begin
            next_newBoard[68 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[78 +: 2] != 0) && (Board[78 +: 2] == Board[80 +: 2] && Board[80 +: 2] == Board[82 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[78 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[80 +: 2] && Board[80 +: 2] == Board[82 +: 2]) && (Board[64 +: 2] != 0)) begin
            next_newBoard[78 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[80 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[82 +: 2]) && (Board[66 +: 2] != 0)) begin
            next_newBoard[80 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[82 +: 2] == 0) && (Board[76 +: 2] != 0) && (Board[76 +: 2] == Board[78 +: 2] && Board[78 +: 2] == Board[80 +: 2]) && (Board[68 +: 2] != 0)) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // \
        else if((Board[34 +: 2] == 0) && (Board[50 +: 2] != 0) && (Board[50 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[82 +: 2])) begin
            next_newBoard[34 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[50 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[66 +: 2] && Board[66 +: 2] == Board[82 +: 2]) && (Board[36 +: 2] != 0)) begin
            next_newBoard[50 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[66 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[82 +: 2]) && (Board[52 +: 2] != 0)) begin
            next_newBoard[66 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[82 +: 2] == 0) && (Board[34 +: 2] != 0) && (Board[34 +: 2] == Board[50 +: 2] && Board[50 +: 2] == Board[66 +: 2]) && (Board[68 +: 2] != 0)) begin
            next_newBoard[82 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        // /
        else if((Board[40 +: 2] == 0) && (Board[52 +: 2] != 0) && (Board[52 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[76 +: 2])) begin
            next_newBoard[40 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[52 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[64 +: 2] && Board[64 +: 2] == Board[76 +: 2]) && (Board[38 +: 2] != 0)) begin
            next_newBoard[52 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[64 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[76 +: 2]) && (Board[50 +: 2] != 0)) begin
            next_newBoard[64 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end
        else if((Board[76 +: 2] == 0) && (Board[40 +: 2] != 0) && (Board[40 +: 2] == Board[52 +: 2] && Board[52 +: 2] == Board[64 +: 2]) && (Board[62 +: 2] != 0)) begin
            next_newBoard[76 +: 2] = `STONE_AI___;
            next_isCritical = 1;
            next_state = OUTP;
            next_valid = 1;
        end

    end // end of task
    endtask

endmodule
