// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01

// define bit width
`define NUM_PARAMS_BITS 25 
`define NUM_CAL_BITS 62

`define SCALING_POW 24

// structure of NN
`define NUM_INPUT 42
`define NUM_HIDDEN_NODE 40
`define NUM_LAYER1_W 1680 // 42*40
`define NUM_LAYER1_B 40
`define NUM_LAYER2_W 40 // 40*1
`define NUM_LAYER2_B 1
`define NUM_TOTAL_PARAMS 1761

// base addr 
`define BASE_LAYER1_W 0
`define BASE_LAYER1_B 1680
`define BASE_LAYER2_W 1720
`define BASE_LAYER2_B 1760

module NN( 
    input wire clk,
    input wire rst,
    input wire req, // when we need to forward the NN, req flip to 1 w/ onepulse
    input wire [0 : 2*`BOARD_H*`BOARD_W-1] Board,
       // Board[row*7*2 + 2*col : row*7*2 + 2*col+1] = Board[row][col:col+1], store serially: [0][0:1], [0][2:3], ..., [5][12:13]
    output wire [`NUM_PARAMS_BITS-1 : 0] y,
    output wire valid_1p
);


    //clock
    wire clk_div2;

    // wire for reading mem of params 
    reg [10:0] addr, next_addr;
    wire [24:0] data;
    reg [10:0] addr_tmp;
    reg [10:0] addr_delay;

    //wire for calculating
    reg [`NUM_CAL_BITS-1 : 0] hidden_node [0 : `NUM_HIDDEN_NODE-1];
    reg [`NUM_CAL_BITS-1 : 0] next_hidden_node [0 : `NUM_HIDDEN_NODE-1];
    reg [`NUM_CAL_BITS-1 : 0] longy, next_longy;



    // output
    reg next_valid;
    reg valid; 

    // FSM
    parameter INIT = 2'b00;
    parameter CALC = 2'b01;
    parameter OUTP = 2'b10;
    reg [1:0] state, next_state;
    
    // use for for-loop
    integer i;

    Clock_divider #(2) c0(.clk_in(clk), .clk_out(clk_div2));
    Onepulse o0(.out(valid_1p), .in(valid), .clock(clk)); // to original clock domain
    blk_mem_gen_NNparams b0(.clka(clk_div2), .wea(1'b0), .addra(addr), .dina(25'b0), .douta(data));

    assign y = longy[`NUM_PARAMS_BITS-1 : 0];

    // delayed signal of addr
    always @ (posedge clk_div2) begin
        addr_tmp <= addr;
        addr_delay <= addr_tmp;
    end

    // DFF
    always @ (posedge clk_div2, posedge rst) begin
        if (rst) begin
            for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) hidden_node[i] = 0;
            state = INIT;
            addr = 0;
            longy = 0;
            valid = 0;
        end
        else begin
            for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) hidden_node[i] = next_hidden_node[i];
            state = next_state;
            addr = next_addr;
            longy = next_longy;
            valid = next_valid;
        end
    end

    // combinational
    always @ (*) begin
        for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) next_hidden_node[i] = hidden_node[i];
        next_state = state;
        next_addr = addr;
        next_longy = longy;
        next_valid = valid;

        case(state)
            INIT: begin
                next_addr = 0;
                next_longy = 0;
                for (i = 0; i < `NUM_HIDDEN_NODE; i = i + 1) next_hidden_node[i] = 0;

                if (req) begin
                    next_state = CALC;
                end
            end
            CALC: begin
                if (addr < `NUM_TOTAL_PARAMS + 2) begin // delay 2 cycle
                    next_addr = addr + 1;

                    if (addr_delay < `NUM_LAYER1_W) begin // layer1 weight
                        // hidden node += w * Xi with sign extension
                        next_hidden_node[ addr_delay/`NUM_INPUT] = hidden_node[ addr_delay/`NUM_INPUT] +
                                                             { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } * 
                                                             {
                                                                { (`NUM_CAL_BITS-1){ Board[ 2 * ( 7*((addr_delay%`NUM_INPUT)%`BOARD_H)+(addr_delay%`NUM_INPUT)/`BOARD_H ) ] } } ,
                                                                Board[2 * ( 7*((addr_delay%`NUM_INPUT)%`BOARD_H)+(addr_delay%`NUM_INPUT)/`BOARD_H ) + 1] 
                                                             } ;
                    end
                    else if (addr_delay < `NUM_LAYER1_W + `NUM_LAYER1_B) begin // layer1 bias & ReLU
                        next_hidden_node[ addr_delay - `BASE_LAYER1_B] = hidden_node[ addr_delay - `BASE_LAYER1_B] + 
                                                                  { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } ;
                        if (next_hidden_node[ addr_delay - `BASE_LAYER1_B][61] == 1)
                            next_hidden_node[ addr_delay - `BASE_LAYER1_B] = 0;
                    end
                    else if (addr_delay < `NUM_LAYER1_W + `NUM_LAYER1_B + `NUM_LAYER2_W) begin // layer2 weight
                        next_longy = longy + 
                                     { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } * 
                                     hidden_node[ addr_delay - `BASE_LAYER2_W ];
                    end
                    else begin // addr_delay == 1760, layer2 bias
                        // shift right first...
                        next_longy = (longy >> `SCALING_POW) + 
                                     { {(`NUM_CAL_BITS-`NUM_PARAMS_BITS){data[`NUM_PARAMS_BITS - 1]}} , data } ;
                    end
                end
                else begin // addr >= `NUM_TOTAL_PARAMS
                    next_state = OUTP;
                    next_valid = 1;
                    next_addr = 0;
                end
            end
            OUTP: begin
                next_state = INIT;
                next_valid = 0;
            end
            default: begin
                next_state = INIT;
            end
        endcase
    end

endmodule

