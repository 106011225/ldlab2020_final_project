// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Jan  5 18:59:49 2021
// Host        : temmiepc running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/liuchinlin/Documents/LDlab2020/final_project/final_project.srcs/sources_1/ip/blk_mem_gen_NNparams/blk_mem_gen_NNparams_stub.v
// Design      : blk_mem_gen_NNparams
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *)
module blk_mem_gen_NNparams(clka, wea, addra, dina, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[10:0],dina[24:0],douta[24:0]" */;
  input clka;
  input [0:0]wea;
  input [10:0]addra;
  input [24:0]dina;
  output [24:0]douta;
endmodule
