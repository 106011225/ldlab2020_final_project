// IO example for Judge module

module Judge (
	input clk,
	input rst,
	inout PS2_CLK,
	inout PS2_DATA,
	input mode,
	output [15:0] LED,
	output [6:0] DISPLAY,
	output [3:0] DIGIT,
	output [3:0] vgaRed,
	output [3:0] vgaGreen,
	output [3:0] vgaBlue
	output hsync,
	output vsync
);
	AI_mod ai1 (req, board, board_AI, rst, clk, AI_valid);		// modify this
	Player player1 (.putStone(putStone), .board_i(board), .board_o(board_player), .valid(player_valid));
	inputDetect detector1 (.PS2_DATA(PS2_DATA), .PS2_CLK(PS2_CLK), .clk(clk), .rst(rst), .putStone(putStone), .en(en));		// clk and rst is for keyboard ctrl
	boardStatus status1 (.board(board), .win(win));
	vga vga1 (.clk(clk), .rst(rst), .en(en), .board(board), .vgaRed(vgaRed), vgaGreen(vgaGreen), vgaBlue(vgaBlue), .hsync(hsync), .vsync(vsync));	// no need clock divider, already instantiated.
	// other additional modules may apply
endmodule