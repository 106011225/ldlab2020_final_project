#  A = [['Window[0:1]', 'Window[2:3]', 'Window[4:5]', 'Window[6:7]'],
#       ['Window[8:9]', 'Window[10:11]', 'Window[12:13]', 'Window[14:15]'],
#       ['Window[16:17]', 'Window[18:19]', 'Window[20:21]', 'Window[22:23]'],
#       ['Window[24:25]', 'Window[26:27]', 'Window[28:29]', 'Window[30:31]']]

#  Window = { Board[ (row*7+col +  0)*2 : (row*7+col +  0)*2+7 ],
#             Board[ (row*7+col +  7)*2 : (row*7+col +  7)*2+7 ],
#             Board[ (row*7+col + 14)*2 : (row*7+col + 14)*2+7 ],
#             Board[ (row*7+col + 21)*2 : (row*7+col + 21)*2+7 ] };



#  A = [ ['Board[ (row*7+col + 0+0)*2 : (row*7+col + 0+0)*2+1 ]',
#         'Board[ (row*7+col + 0+1)*2 : (row*7+col + 0+1)*2+1 ]', 
#         'Board[ (row*7+col + 0+2)*2 : (row*7+col + 0+2)*2+1 ]', 
#         'Board[ (row*7+col + 0+3)*2 : (row*7+col + 0+3)*2+1 ]'],
#        ['Board[ (row*7+col + 7+0)*2 : (row*7+col + 7+0)*2+1 ]', 
#         'Board[ (row*7+col + 7+1)*2 : (row*7+col + 7+1)*2+1 ]', 
#         'Board[ (row*7+col + 7+2)*2 : (row*7+col + 7+2)*2+1 ]', 
#         'Board[ (row*7+col + 7+3)*2 : (row*7+col + 7+3)*2+1 ]'],
#        ['Board[ (row*7+col + 14+0)*2 : (row*7+col + 14+0)*2+1 ]', 
#         'Board[ (row*7+col + 14+1)*2 : (row*7+col + 14+1)*2+1 ]', 
#         'Board[ (row*7+col + 14+2)*2 : (row*7+col + 14+2)*2+1 ]', 
#         'Board[ (row*7+col + 14+3)*2 : (row*7+col + 14+3)*2+1 ]'],
#        ['Board[ (row*7+col + 21+0)*2 : (row*7+col + 21+0)*2+1 ]', 
#         'Board[ (row*7+col + 21+1)*2 : (row*7+col + 21+1)*2+1 ]', 
#         'Board[ (row*7+col + 21+2)*2 : (row*7+col + 21+2)*2+1 ]', 
#         'Board[ (row*7+col + 21+3)*2 : (row*7+col + 21+3)*2+1 ]']]

#  B = [ ['next_newBoard[ (row*7+col + 0+0)*2 : (row*7+col + 0+0)*2+1 ]',
#         'next_newBoard[ (row*7+col + 0+1)*2 : (row*7+col + 0+1)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 0+2)*2 : (row*7+col + 0+2)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 0+3)*2 : (row*7+col + 0+3)*2+1 ]'],
#        ['next_newBoard[ (row*7+col + 7+0)*2 : (row*7+col + 7+0)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 7+1)*2 : (row*7+col + 7+1)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 7+2)*2 : (row*7+col + 7+2)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 7+3)*2 : (row*7+col + 7+3)*2+1 ]'],
#        ['next_newBoard[ (row*7+col + 14+0)*2 : (row*7+col + 14+0)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 14+1)*2 : (row*7+col + 14+1)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 14+2)*2 : (row*7+col + 14+2)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 14+3)*2 : (row*7+col + 14+3)*2+1 ]'],
#        ['next_newBoard[ (row*7+col + 21+0)*2 : (row*7+col + 21+0)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 21+1)*2 : (row*7+col + 21+1)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 21+2)*2 : (row*7+col + 21+2)*2+1 ]', 
#         'next_newBoard[ (row*7+col + 21+3)*2 : (row*7+col + 21+3)*2+1 ]']]


for row in range(3):
    for col in range(4):

        A = [ [f'Board[{(row*7+col +  0+0)*2:2d} +: 2]',
               f'Board[{(row*7+col +  0+1)*2:2d} +: 2]', 
               f'Board[{(row*7+col +  0+2)*2:2d} +: 2]', 
               f'Board[{(row*7+col +  0+3)*2:2d} +: 2]'],
              [f'Board[{(row*7+col +  7+0)*2:2d} +: 2]', 
               f'Board[{(row*7+col +  7+1)*2:2d} +: 2]', 
               f'Board[{(row*7+col +  7+2)*2:2d} +: 2]', 
               f'Board[{(row*7+col +  7+3)*2:2d} +: 2]'],
              [f'Board[{(row*7+col + 14+0)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 14+1)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 14+2)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 14+3)*2:2d} +: 2]'],
              [f'Board[{(row*7+col + 21+0)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 21+1)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 21+2)*2:2d} +: 2]', 
               f'Board[{(row*7+col + 21+3)*2:2d} +: 2]']]

        B = [ [f'next_newBoard[{(row*7+col +  0+0)*2:2d} +: 2]',
               f'next_newBoard[{(row*7+col +  0+1)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col +  0+2)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col +  0+3)*2:2d} +: 2]'],
              [f'next_newBoard[{(row*7+col +  7+0)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col +  7+1)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col +  7+2)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col +  7+3)*2:2d} +: 2]'],
              [f'next_newBoard[{(row*7+col + 14+0)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 14+1)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 14+2)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 14+3)*2:2d} +: 2]'],
              [f'next_newBoard[{(row*7+col + 21+0)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 21+1)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 21+2)*2:2d} +: 2]', 
               f'next_newBoard[{(row*7+col + 21+3)*2:2d} +: 2]']]
    


        #  vertical
        print('// vertical')
        for i in [0, 3]:
            print(f'if(({A[3][i]:s} == 0) && ({A[0][i]:s} != 0) && ({A[0][i]:s} == {A[1][i]:s} && {A[1][i]:s} == {A[2][i]:s})) begin')
            print(f'    {B[3][i]:s} = `STONE_AI___;')
            print(f'    next_isCritical = 1;')
            print(f'    next_state = OUTP;')
            print(f'    next_valid = 1;')
            print(f'end')

        #  horizontal
        print('// horizontal')
        for i in range(4):
            if (i == 0):
                print(f'else if(({A[i][0]:s} == 0) && ({A[i][1]:s} != 0) && ({A[i][1]:s} == {A[i][2]:s} && {A[i][2]:s} == {A[i][3]:s})) begin') 
                print(f'    {B[i][0]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][1]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][2]:s} && {A[i][2]:s} == {A[i][3]:s})) begin') 
                print(f'    {B[i][1]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][2]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][1]:s} && {A[i][1]:s} == {A[i][3]:s})) begin') 
                print(f'    {B[i][2]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][3]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][1]:s} && {A[i][1]:s} == {A[i][2]:s})) begin') 
                print(f'    {B[i][3]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
            else:
                print(f'else if(({A[i][0]:s} == 0) && ({A[i][1]:s} != 0) && ({A[i][1]:s} == {A[i][2]:s} && {A[i][2]:s} == {A[i][3]:s}) && ({A[i-1][0]:s} != 0)) begin') 
                print(f'    {B[i][0]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][1]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][2]:s} && {A[i][2]:s} == {A[i][3]:s}) && ({A[i-1][1]:s} != 0)) begin') 
                print(f'    {B[i][1]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][2]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][1]:s} && {A[i][1]:s} == {A[i][3]:s}) && ({A[i-1][2]:s} != 0)) begin') 
                print(f'    {B[i][2]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')
                print(f'else if(({A[i][3]:s} == 0) && ({A[i][0]:s} != 0) && ({A[i][0]:s} == {A[i][1]:s} && {A[i][1]:s} == {A[i][2]:s}) && ({A[i-1][3]:s} != 0)) begin') 
                print(f'    {B[i][3]:s} = `STONE_AI___;')
                print(f'    next_isCritical = 1;')
                print(f'    next_state = OUTP;')
                print(f'    next_valid = 1;')
                print(f'end')

        #  \
        print('// \\')
        print(f'else if(({A[0][0]:s} == 0) && ({A[1][1]:s} != 0) && ({A[1][1]:s} == {A[2][2]:s} && {A[2][2]:s} == {A[3][3]:s})) begin') 
        print(f'    {B[0][0]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[1][1]:s} == 0) && ({A[0][0]:s} != 0) && ({A[0][0]:s} == {A[2][2]:s} && {A[2][2]:s} == {A[3][3]:s}) && ({A[0][1]:s} != 0)) begin') 
        print(f'    {B[1][1]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[2][2]:s} == 0) && ({A[0][0]:s} != 0) && ({A[0][0]:s} == {A[1][1]:s} && {A[1][1]:s} == {A[3][3]:s}) && ({A[1][2]:s} != 0)) begin') 
        print(f'    {B[2][2]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[3][3]:s} == 0) && ({A[0][0]:s} != 0) && ({A[0][0]:s} == {A[1][1]:s} && {A[1][1]:s} == {A[2][2]:s}) && ({A[2][3]:s} != 0)) begin') 
        print(f'    {B[3][3]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')

        #  /
        print('// /')
        print(f'else if(({A[0][3]:s} == 0) && ({A[1][2]:s} != 0) && ({A[1][2]:s} == {A[2][1]:s} && {A[2][1]:s} == {A[3][0]:s})) begin') 
        print(f'    {B[0][3]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[1][2]:s} == 0) && ({A[0][3]:s} != 0) && ({A[0][3]:s} == {A[2][1]:s} && {A[2][1]:s} == {A[3][0]:s}) && ({A[0][2]:s} != 0)) begin') 
        print(f'    {B[1][2]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[2][1]:s} == 0) && ({A[0][3]:s} != 0) && ({A[0][3]:s} == {A[1][2]:s} && {A[1][2]:s} == {A[3][0]:s}) && ({A[1][1]:s} != 0)) begin') 
        print(f'    {B[2][1]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')
        print(f'else if(({A[3][0]:s} == 0) && ({A[0][3]:s} != 0) && ({A[0][3]:s} == {A[1][2]:s} && {A[1][2]:s} == {A[2][1]:s}) && ({A[2][0]:s} != 0)) begin') 
        print(f'    {B[3][0]:s} = `STONE_AI___;')
        print(f'    next_isCritical = 1;')
        print(f'    next_state = OUTP;')
        print(f'    next_valid = 1;')
        print(f'end')

