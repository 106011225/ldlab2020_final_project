`timescale 1ns / 100ps

// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01

module NN_t;
    parameter CYC = 100;
    
    reg clk = 0;
    reg rst = 0;
    reg req = 0;
        // Board[row*7*2 + 2*col : row*7*2 + 2*col+1] = Board[row][col:col+1], store serially: [0][0:1], [0][2:3], ..., [5][12:13]
    reg [0 : 2*`BOARD_H*`BOARD_W-1] Board; 
    wire [`NUM_PARAMS_BITS-1 : 0] y;
    reg [`NUM_PARAMS_BITS-1 : 0] ans = 0;
    wire valid;

        NN nn0(.clk(clk), .rst(rst), .req(req), .Board(Board), .y(y), .valid_1p(valid));

        always
            #(CYC/2) clk = ~clk;


        initial begin
            Board = {`STONE_BLANK, `STONE_AI___, `STONE_HUMAN, `STONE_HUMAN, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                     `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_AI___, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                     `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                     `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                     `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                     `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK };
    end



    initial begin
        #CYC rst = 1;
        #CYC rst = 0;

        #(151) req = 1;
        #(CYC*4) req = 0;
        #(1510000) req = 1;
        #(CYC*4) req = 0;
    end

    always @ (posedge clk) begin
        if (valid) 
            ans = y;
    end


endmodule

// module blk_mem_gen_NNparams(clka, wea, addra, dina, douta)
// [> synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[10:0],dina[24:0],douta[24:0]" <];
//   input clka;
//   input [0:0]wea;
//   input [10:0]addra;
//   input [24:0]dina;
//   output [24:0]douta;
// endmodule
