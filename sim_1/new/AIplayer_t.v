`timescale 1ns / 100ps

// shape of Board
`define BOARD_H 6
`define BOARD_W 7

// stone on Board
`define STONE_BLANK 2'b00 
`define STONE_AI___ 2'b11
`define STONE_HUMAN 2'b01


module AIplayer_t;
    parameter CYC = 100;

    reg clk = 0;
    reg rst = 0;
    reg req = 0;

    reg [0 : 2*`BOARD_H*`BOARD_W-1] Board; 
    wire [0 : 2*`BOARD_H*`BOARD_W-1] newBoard; 
    reg [0 : 2*`BOARD_H*`BOARD_W-1] nextBoard; 
    wire valid;
    wire [2:0] state;
    wire [2:0] testingCol;
    wire reqNN;
    wire [`NUM_PARAMS_BITS-1 : 0] y;
    wire [`NUM_PARAMS_BITS-1 : 0] currMax;
    wire [0 : 2*`BOARD_H*`BOARD_W-1] NNinputBoard;

    AIplayer a0(clk, rst, req, Board, newBoard, valid, state, testingCol, reqNN, y, currMax, NNinputBoard);

    always
        #(CYC/2) clk = ~clk;


    initial begin
        Board = {`STONE_BLANK, `STONE_BLANK, `STONE_HUMAN, `STONE_HUMAN, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                 `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_AI___, `STONE_HUMAN, `STONE_BLANK, `STONE_BLANK,
                 `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_HUMAN, `STONE_BLANK,
                 `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                 `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK,
                 `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK, `STONE_BLANK };
    end

    initial begin
        #CYC rst = 1;
        #CYC rst = 0;

        #(151) req = 1;
        #(CYC*4) req = 0;
    end

    always @ (posedge clk) begin
        if (valid) 
            nextBoard = newBoard;
    end


endmodule

// module AIplayer(
//     input wire clk,
//     input wire rst,
//     input wire req,
//     input wire [0 : 2*`BOARD_H*`BOARD_W-1] Board,
//     output reg [0 : 2*`BOARD_H*`BOARD_W-1] newBoard,
//     output reg valid
// );
